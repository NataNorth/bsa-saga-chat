package com.chatserver.users.model;

import com.chatserver.users.dto.UserDetailsDto;
import lombok.*;
import javax.persistence.*;

@Entity
@Data
@Builder
public class User {
    private String id;
    private String email;
    private String username;
    private String password;
    private String avatar;

    static public User toUserEntity(UserDetailsDto userDto) {
        return User.builder()
                .id(String.valueOf(userDto.getId()))
                .email(userDto.getEmail())
                .username(userDto.getUsername())
                .build();
    }
}
