package com.chatserver.users;

import com.chatserver.users.model.User;
import com.google.gson.Gson;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UsersCache {
    private final Logger logger = LoggerFactory.getLogger(UsersCache.class);

    private final Map<String, User> cache = new HashMap<>();

    @Autowired
    private UsersCache() {
        try {
            Path usersFile = Path.of("src/main/resources/static/users.json");
            String jsonString = Files.readString(usersFile);
            Gson g = new Gson();
            Type collectionType = new TypeToken<Collection<User>>(){}.getType();
            Collection<User> users = g.fromJson(jsonString, collectionType);
            users.forEach(user -> cache.put(user.getId(), user));
        } catch (IOException e) {
            logger.error("UserCache failed to initialize " + e);
        }
    }

    public List<User> getAllUsers() {
        return new ArrayList<>(cache.values());
    }

    public Optional<User> findById(String id) {
        if (cache.containsKey(id)) {
            return Optional.of(cache.get(id));
        } else {
            return Optional.empty();
        }
    }

    public Optional<User> findByEmail(String email) {
        AtomicReference<User> result = null;
        cache.forEach((id, user) -> {
            if (user.getEmail().equals(email)) {
            result.set(user);
        }});
        return Optional.of(result.get());
    }

    public void save(User user) {
        cache.put(user.getId(), user);
    }

    public void deleteById(String id) {
        cache.remove(id);
    }

    public void edit(User user) {
        cache.replace(user.getId(), user);
    }
}
