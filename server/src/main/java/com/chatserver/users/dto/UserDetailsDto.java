package com.chatserver.users.dto;

import lombok.Data;
import java.util.UUID;

@Data
public class UserDetailsDto {
    private UUID id;
    private String email;
    private String username;
    private String image;
}
