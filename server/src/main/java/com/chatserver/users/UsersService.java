package com.chatserver.users;

import com.chatserver.users.dto.UserDetailsDto;
import com.chatserver.users.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UsersService {
    @Autowired
    private UsersCache usersRepository;

    public static String getUserId(){
        UUID uuid = UUID.randomUUID();
        return String.valueOf(uuid);
    }

    public UserDetailsDto getUserById(String id) {
        return usersRepository
                .findById(id)
                .map(UserMapper.MAPPER::userToUserDetailsDto)
                .orElseThrow(() -> new UsernameNotFoundException("No user found with username"));
    }

    public void save(User user) {
        usersRepository.save(user);
    }

}
