package com.chatserver.users;

import com.chatserver.message.model.Message;
import com.chatserver.users.dto.UserDetailsDto;
import com.chatserver.users.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UsersService usersService;

    @RequestMapping("/")
    public void handler() {
        Authentication auth = SecurityContextHolder.getContext()
                .getAuthentication();
        System.out.println("user: "+ auth.getName());
        System.out.println("roles: "+ auth.getAuthorities());
    }

    @GetMapping
    public UserDetailsDto getUser() {
        return usersService.getUserById(usersService.getUserId());
    }

    @GetMapping("/{id}")
    public UserDetailsDto getUserById(@PathVariable String id) {
        return usersService.getUserById(id);
    }


    @PostMapping
    public void postUser(@RequestBody UserDetailsDto user) {
        usersService.save(User.toUserEntity(user));
    }

    @DeleteMapping("/id")
    public void delete(@PathVariable String id) {

    }
    @PostMapping("/{id}")
    private void edit(@PathVariable String id, @RequestBody User user) { }
}
