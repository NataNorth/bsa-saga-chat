package com.chatserver.message;

import com.chatserver.message.model.Message;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class MessagesCache {
    private final Logger logger = LoggerFactory.getLogger(MessagesCache.class);

    private final Map<String, Message> cache = new HashMap<>();

    @Autowired
    private MessagesCache() {
        try {
            Path messagesFile = Path.of("src/main/resources/static/messages.json");
            String jsonString = Files.readString(messagesFile);
            Gson g = new Gson();
            Type collectionType = new TypeToken<Collection<Message>>(){}.getType();
            Collection<Message> messages = g.fromJson(jsonString, collectionType);
            messages.forEach(message -> cache.put(message.getId(), message));
        } catch (IOException e) {
            logger.error("MessageCache failed to initialize " + e);
        }
    }

    public List<Message> getAll() {
        return new ArrayList<>(cache.values());
    }

    public Message getById(String id) {
        return cache.get(id);
    }

    public void add(Message message) {
        cache.put(message.getId(), message);
    }

    public void edit(String id, Message message) {
        cache.replace(id, message);
    }

    public void delete(String id) {
        cache.remove(id);
    }
}
