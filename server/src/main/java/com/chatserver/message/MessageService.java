package com.chatserver.message;

import com.chatserver.message.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MessageService {

    @Autowired
    MessagesCache messagesCache;

    public List<Message> getAll() {
        return messagesCache.getAll();
    }

    public Message getById(String id) {
        return messagesCache.getById(id);
    }

    public void add(Message message) {
        messagesCache.add(message);
    }

    public void edit(String id, Message message) {
        messagesCache.edit(id, message);
    }

    public void delete(String id) {
        messagesCache.delete(id);
    }
}
