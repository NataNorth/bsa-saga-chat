package com.chatserver.message;

import com.chatserver.message.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/message")
public class MessageController {

    private final MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping
    private ResponseEntity<List<Message>> getAll() {
        return ResponseEntity.status(HttpStatus.OK).body(messageService.getAll());
    }

    @GetMapping("/{id}")
    private ResponseEntity<Message> getById(@PathVariable String id) {
        return ResponseEntity.status(HttpStatus.OK).body(messageService.getById(id));
    }

    @PostMapping
    private void add(@RequestBody Message message) {
        messageService.add(message);
    }

    @PostMapping("/{id}")
    private void edit(@PathVariable String id, @RequestBody Message message) {
        messageService.edit(id, message);
    }

    @DeleteMapping("/{id}")
    private void delete(@PathVariable String id) {
        messageService.delete(id);
    }
}
