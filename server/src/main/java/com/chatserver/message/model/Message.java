package com.chatserver.message.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Message {
    private String id;
    private String text;
    private String user;
    private String avatar;
    private String userId;
    private String editedAt;
    private String createdAt;
}
