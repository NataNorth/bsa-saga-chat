import { combineReducers } from "@reduxjs/toolkit";
import data from '../components/messageList/reducer';
import editForm from '../components/editForm/reducer';
import auth from '../components/auth/reducer'

export default combineReducers({
    data,
    editForm,
    auth
});