import messagesSagas from '../components/messageList/sagas'
import editFormSagas from '../components/editForm/sagas'
import watchAuthSaga from '../components/auth/sagas'
import { all } from 'redux-saga/effects'

export default function* rootSaga() {
    yield all([
        messagesSagas(),
        editFormSagas(),
        watchAuthSaga()
    ])
}