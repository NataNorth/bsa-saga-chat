import React from 'react';
import { Switch, Route } from 'react-router';
import Chat from './components/chat/chat';
import EditForm from './components/editForm/index'
import ErrorPage from './components/errorPage';
import LoginForm from './components/auth/index.js'

function App() {
  return (
    <div className="App">  
      <Switch>
          <Route exact path="/login" component={LoginForm}/>
          <Route exact path="/" component={Chat}/>
          <Route exact path="/edit/:id" component={EditForm}/>
          <Route path="/error" component={ErrorPage}/>
      </Switch>
    </div>
);
}

export default App;