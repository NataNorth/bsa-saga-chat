import React from 'react';
import './index.css'
import { connect } from 'react-redux'
import { deleteMessage } from '../actions';
import service from '../../chat/service'

class Message extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            text: ''
        }
        this.handleClick = this.handleClick.bind(this);
    }

    componentDidMount() {
        if (this.props.currentUserId === this.props.message.userId) {
            var messageItem = document.getElementById(this.props.message.id);
            messageItem.classList.add("currentUser")
        }
        this.setState({
            text: this.props.message.text
        })
    }

    componentDidUpdate() {
        if (this.props.currentUserId === this.props.message.userId) {
            var messageItem = document.getElementById(this.props.message.id);
            messageItem.classList.add("currentUser")
        }
    }

    onDelete(e){
        e.preventDefault();
        this.props.deleteMessage(this.props.message.id);
        this.setState({
            text: ''
        });
    }

    onEdit(e, id) {
        e.preventDefault();
        this.props.history.push(`/edit/${id}`)
    }

    handleClick(e) {
        e.preventDefault();
        if (this.props.currentUserId !== this.props.message.userId) {
            e.target.classList.toggle('active');
        }
    }

    render() {
        return (
            <div className='message' id={this.props.message.id}>
                {this.props.currentUserId !== this.props.message.userId ?
                <div>
                    <img src = {this.props.message.avatar} alt='avatar'/>
                </div>  : <br/>}
                <div className='messageText'>
                    <span>{this.props.message.text}</span>
                </div>
                <div className='createdAt'>
                    {service.getHours(this.props.message.createdAt)}
                </div>
                <button className="ui icon button" className='likeButton'
                        onClick={this.handleClick}>
                    <i className="heart icon"></i>
                </button>
                {this.props.currentUserId === this.props.message.userId ?
                    <div className='mutationForm'>
                        <button 
                            name="edit"
                            onClick={(e) => this.onEdit(e, this.props.message.id)}
                        >Edit</button>
                        <button 
                            name='delete'
                            onClick={(e) => this.onDelete(e)}
                        >Delete</button>
                    </div> : <div/>}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    currentUserId: state.data.currentUser.id,
    messageId: state.editForm.messageId
});

const mapDispatchToProps = dispatch => {
    return {
        deleteMessage: id => dispatch(deleteMessage(id)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Message);