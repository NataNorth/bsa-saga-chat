import api from '../../shared/config/api'
import axios from 'axios'
import { call, put, takeEvery, all} from 'redux-saga/effects'
import * as actions from './actions'
import * as types from './ActionTypes'

export function* fetchMessages() {
    try {
        yield put(actions.fetchMessagesBegin());
        const messages = yield call(() => axios.get(`${api.url}/message`, {auth: {username: "admin", password: "admin"}}));
        var sortedMessages = messages.sort((a,b) => (a.createdAt > b.createdAt) ? 1 : ((b.createdAt > a.createdAt) ? -1 : 0));
        yield put(actions.fetchMessagesSuccess(sortedMessages));
    } catch (error) {
        yield put(actions.fetchMessagesFailure(error));
    }
}

function* watchFetchMessages() {
    yield takeEvery(types.FETCH_MESSAGES, fetchMessages)
}

export function* addMessage(action) {
    const { id, text, createdAt } = action.payload;
        var newMessage = {
          id: id,
          text: text,
          user: localStorage.currentUser.name,
          avatar: localStorage.currentUser.avatar,
          userId: localStorage.currentUser.id,
          editedAt: "",
          createdAt: createdAt
        }
    try {
        yield call(axios.post, `${api.url}/message`, newMessage);
        yield put (actions.fetchMessages());
    } catch(error) {
        yield put(actions.fetchMessagesFailure(error));
    }
}

function* watchAddMessage() {
    yield takeEvery(types.ADD_MESSAGE, addMessage)
}

export function* editMessage(action) {
    const { id, text, editedAt } = action.payload;
    var editedMessage = localStorage.messages.filter(message => message.id === id); 
    editedMessage.text = text;
    editedMessage.editedAt = editedAt;
    try {
        yield call(axios.put, `${api.url}/messages/${id}`, editedMessage);
        yield put(actions.fetchMessages());
    } catch (error) {
        yield put(actions.fetchMessagesFailure(error));
    }
}

function* watchEditMessage() {
    yield takeEvery(types.EDIT_MESSAGE, editMessage)
}

export function* deleteMessage(action) {
    var id = action.payload.id;
    try {
        yield call(axios.delete, `${api.url}/message/${id}`);
        yield put(actions.fetchMessages());
    } catch (error) {
        yield put(actions.fetchMessagesFailure(error));
    }
}

function* watchDeleteMessage() {
    yield takeEvery(types.DELETE_MESSAGE, deleteMessage);
}

export default function* messagesSagas() {
    yield all([
        watchFetchMessages(),
        watchAddMessage(),
        watchDeleteMessage(),
        watchEditMessage()
    ])
}