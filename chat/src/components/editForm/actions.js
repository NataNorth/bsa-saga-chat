import * as types from './actionTypes'

export const fetchMessageSuccess = message => ({
    type: types.FETCH_MESSAGE_SUCCESS,
    payload: {
        message
    }
})

export const fetchMessageFailure = error => ({
    type: types.FETCH_MESSAGE_FAILURE
})