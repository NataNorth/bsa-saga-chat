import { call, put, takeEvery } from "redux-saga/effects";
import * as actions from './actions';
import { FETCH_MESSAGE } from "./actionTypes";
import axios from 'axios';
import api from '../../shared/config/api';

export function* fetchMessage(action) {
    try {
        const message = yield call(axios.get, `${api.url}/message/${action.payload.id}`);
        yield put(actions.fetchMessageSuccess(message));
    } catch (error) {
        yield put(actions.fetchMessageFailure(error));
    }
}

function* watchFetchMessage() {
    yield takeEvery(FETCH_MESSAGE, fetchMessage);
}

export default function* editFormSagas() {
    yield watchFetchMessage();
}