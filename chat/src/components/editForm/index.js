import React from 'react';
import './index.css'
import { connect } from 'react-redux'
import { editMessage } from '../messageList/actions'

class EditForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
           id: '',
           text: ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.getEditForm = this.getEditForm.bind(this);
        this.onCancel = this.onCancel.bind(this);
    }

    componentDidMount() {
        if (this.props.match.params.id) {
            this.props.fetchMessage(this.props.match.params.id);
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if(nextProps.message.id !== prevState.id && nextProps.match.params.id) {
            return {
                ...nextProps.message.id,
                ...nextProps.message.text
            }
        } else {
            return null;
        }
    }
    // static getDerivedStateFromProps(nextProps, prevState) {
    //     if(nextProps.messageId !== prevState.id && nextProps.isShown) {
    //         var currentMessage = nextProps.messages.find(message => message.id === nextProps.messageId);
    //         console.log('next ' + currentMessage.text + ' prev ' + prevState)
    //         console.log(prevState)
    //         if (!prevState.text) {
    //             return {
    //                 text: currentMessage.text
    //             };
    //         } else {
    //             return {
    //                 text: prevState.text
    //             }
    //         }
    //     } else {
    //         return {
    //             text: ''
    //         }
    //     }
    // }

    handleSubmit(e){
        e.preventDefault();
        this.props.editMessage(this.props.messageId, this.state.text);
        this.props.history.push("/");
    }

    handleChange(e) {
        e.preventDefault();
        this.setState({text: e.target.value});
    }

    onCancel() {
        this.setState({
            id: '',
            text: ''
        });
        this.props.history.push("/");
    }

    getEditForm() {
        let data = this.state;
        return (
            <div className = "editForm">
                <form 
                    onSubmit={this.handleSubmit} >
                        <textarea
                        id='editFormTextarea'
                        onChange={this.handleChange}
                        value={data.text} />
                    <div className="buttons">
                        <button type='submit'>Submit</button>
                        <button type='button'
                        onClick={this.onCancel}>Cancel</button>
                    </div>
                </form>
            </div>
        )
    }

    render() {
        return !this.props.loading ? this.getEditForm() : null;
    }
}

const mapStateToProps = state => {
    return {
        message: state.editForm.message,
        loading: state.editForm.loading
    }
}

const mapDispatchToProps = dispatch => {
    return {
        editMessage: (id, text) => dispatch(editMessage(id, text)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (EditForm);