import * as types from './actionTypes'

const initialState = {
    message: {},
    loading: true,
    error: null
}

export default function reducer(state = initialState, action) {
    switch(action.type) {
        case types.FETCH_MESSAGE_SUCCESS: {
            return {
                ...state, 
                loading: false,
                message: action.payload.message
            }
        }
        case types.FETCH_MESSAGE_FAILURE: {
            return {
                ...state,
                loading: false,
                error: action.payload.error
            }
        }

        default:
            return state;
        
    }
}