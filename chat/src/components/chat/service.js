
const getNewId = () => {
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g,c=>(c^crypto.getRandomValues(new Uint8Array(1))[0]&15 >> c/4).toString(16));
}

const getTime = () => {
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date+'T'+time+'Z';
    return dateTime;
}

const getHours = (currTime) => {
    var time = currTime.split('T')[1].slice(0, -1).split('.')[0].slice(0, -3);
    return time;
}

export default {
    getNewId,
    getTime,
    getHours
};