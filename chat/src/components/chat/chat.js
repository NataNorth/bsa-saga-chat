import React from 'react';
import './chat.css';
import MessageList from '../messageList/index'
import MessageInput from '../messageInput/index'
import Header from '../header/index'
import EditForm from '../editForm/index'
import { Switch, Route } from 'react-router';

function Chat() {
  return (
    <div className="chat">
      <Header />
      <MessageList />
      <MessageInput />
    </div>
);
}

export default Chat;
