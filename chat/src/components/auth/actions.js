export const login = (email, password, signin) => {
    return {
        type: 'AUTH_USER',
        email: email,
        password: password,
        signin: signin
    }
};

export const authStart = () => {
    return {
        type: 'AUTH_START'
    }
};

export const authSuccess = (token, userId) => {
    return {
        type: 'AUTH_SUCCESS',
        token: token,
        userId: userId
    }
};

export const authFail = (error) => {
  return {
      type: 'AUTH_FAIL',
      error: error
  }
};

export const logout = () => {
    return {
        type: 'AUTH_INITIATE_LOGOUT'
    }
};

export const logoutSucceed = () => {
    return {
        type: 'AUTH_LOGOUT'
    }
};

export const checkAuthTimeout = (expirationTime) => {
    return {
        type: 'AUTH_CHECK_TIMEOUT',
        expirationTime: expirationTime
    }
};