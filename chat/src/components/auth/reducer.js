import * as actionTypes from './actionTypes';

const INITIAL_STATE = {
    token: null,
    userId: null,
    error: null,
    loading: false,
};


const reducer = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case actionTypes.AUTH_START: 
            return {...state, ...{ loading: true, error: null }};
        case actionTypes.AUTH_SUCCESS:
            return {...state, token: action.token, userId: action.userId, loading: false };
        case actionTypes.AUTH_FAIL:
            return {...state,  loading: false, error: action.error };
        case actionTypes.AUTH_LOGOUT:
            return {...state, token: null, userId: null };
        default: return state;
    }
};

export default reducer;