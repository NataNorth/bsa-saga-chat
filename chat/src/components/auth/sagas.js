import {put, call, takeEvery} from 'redux-saga/effects';
import axios from "axios";
import api from '../../shared/config/api';

import * as actions from './actions'

export default function* watchAuthSaga() {
    yield takeEvery('AUTH_USER', authUser);
    yield takeEvery('AUTH_INITIATE_LOGOUT', logout);
}

function* authUser(action) {
    yield put(actions.authStart());
    try {
        const response = yield call(() => axios.post(api.url, { email: action.name, password: action.password}));
        const expirationDate = new Date(new Date().getTime() + response.data.expiresIn * 1000);
        yield localStorage.setItem('token', response.data.token);
        yield localStorage.setItem('expirationDate', expirationDate);
        yield localStorage.setItem('userId', response.data.userId);
        yield put(actions.authSuccess(response.data.token, response.data.userId));
    } catch (error) {
        yield put(actions.authFail(error))
    }
}


function* logout(action) {
    yield localStorage.removeItem('token');
    yield localStorage.removeItem('expirationDate');
    yield localStorage.removeItem('userId');
    yield put(actions.logoutSucceed())
}