import React from 'react';
import PropTypes from 'prop-types';

const ErrorPage = ({error}) => {
    return (
        <div className="errorPage">
            {error ? <span>{error}</span> : null}
        </div>
    )
}


export default ErrorPage;