import { createStore, applyMiddleware, compose } from '@reduxjs/toolkit';
import chat from '../reducers/chat';
import createSagaMiddleware from 'redux-saga';
import rootSaga from "../sagas/index"

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const sagaMiddleware = createSagaMiddleware();

export default createStore(
    chat,
    composeEnhancer(applyMiddleware(sagaMiddleware))
);

sagaMiddleware.run(rootSaga)
